#
# Cookbook Name:: enabling-platform
# Recipe:: apache-tomcat-v7
#
# Copyright 2010-2015, Company, Country, http://url/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Company", http://url/ .
#
# Developers:
#  * Firstname Lastname <email@domain>
#
package "mos-apache-tomcat-v7"

cookbook_file "apache-tomcat-v7-tomcat-users.xml" do
   path "/opt/mos-apache-tomcat-v7/conf/tomcat-users.xml"
   action :create
end

cookbook_file "apache-tomcat-v7-server.xml" do
   path "/opt/mos-apache-tomcat-v7/conf/server.xml"
   action :create
end

execute 'start-mos-apache-tomcat-v7' do
  not_if do 
    ::File.exists?("/var/run/mos-apache-tomcat-v7.pid")
  end
  command '/opt/mos-apache-tomcat-v7/cmd/bootstrap'
  action :run
end