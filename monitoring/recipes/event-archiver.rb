#
# Cookbook Name:: monitoring
# Recipe:: specs-monitoring-event-archiver
#
# Copyright 2010-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Institute e-Austria, Timisoara", http://www.ieat.ro/ .
#
# Developers:
#  * Silviu Panica <silviu@solsys.ro>, <silviu.panica@e-uvt.ro>
#

package "specs-monitoring-event-archiver"

include_recipe "monitoring::event-hub"  

ruby_block "event_archiver_configuration" do
  block do
  open('/opt/specs-monitoring-event-hub/etc/conf.d/default-event-hub.toml', 'a') { |f|
    f << "\n[hekad.archiver]\n"
    f << "address = \"http://localhost:10101/monitoring/events\"\n"
  }
  end
  only_if { ::File.exists?("/opt/specs-monitoring-event-hub/etc/conf.d/default-event-hub.toml") and
            ! File.open('/opt/specs-monitoring-event-hub/etc/conf.d/default-event-hub.toml').read().include? "[hekad.archiver]"
          }
  notifies :restart, 'service[specs-monitoring-event-hub_control]', :immediately
end



# start the service using Systemd (applicable only for services that have already a systemd unit file registered)
service "specs-monitoring-event-archiver_start" do
  not_if do ::File.exists?("/opt/specs-monitoring-event-archiver/var/run/event-archiver.pid") end
  provider Chef::Provider::Service::Systemd
  service_name "specs-monitoring-event-archiver"
  action :start
end

# start the service using Systemd (applicable only for services that have already a systemd unit file registered)
service "specs-monitoring-event-hub_control" do
  provider Chef::Provider::Service::Systemd
  service_name "specs-monitoring-event-hub"
  action :nothing
  supports :restart => true
end

# register the event-hub into the DNS (TODO: call this only if service startup is successful)
#
hostname='event-archiver.monitoring.services'
#
### DO NOT MODIFY BELOW THIS LINE
bash "ns_register" do
    user "root"
    cwd "/tmp"
    code <<-EOH
    if [ -f /mos/lib/mos/tools.sh ];then
        source /mos/lib/mos/tools.sh
        ns_register_service A #{hostname}
    fi
    EOH
    returns [0, 1]
    not_if "source /mos/lib/mos/tools.sh; ns_check_service #{hostname}"
end
## end register into the DNS