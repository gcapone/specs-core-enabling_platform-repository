name             'sla-negotiation'
maintainer       'SPECS Project -- Layer/Group'
maintainer_email 'support@specs-project.eu'
license          'Apache 2.0'
description      'Chef Cookbook to manage the deployment of various SPECS SLA Negotiation Components'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.1'

depends 'enabling-platform'